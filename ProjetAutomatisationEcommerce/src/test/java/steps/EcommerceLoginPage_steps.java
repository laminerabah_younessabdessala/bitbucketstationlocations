package steps;

import static org.junit.Assert.fail;
import io.cucumber.datatable.DataTable;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.*;
import io.github.bonigarcia.wdm.WebDriverManager;

public class EcommerceLoginPage_steps {
	WebDriver driver;
	String url="http:tutorialsninja.com/demo/index.php?route=account/login";

	@Given("je suis dans la page Login de Ecommerce")
	public void je_suis_dans_la_page_Login() {
		WebDriverManager.chromedriver().setup();
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get(url);
	}

	@When("je me connecte avec les logins")
	public void je_rentre_mes_logins(DataTable datatable) {
		
		driver.findElement(By.id("input-email")).sendKeys(datatable.cell(1, 0));
		driver.findElement(By.id("input-password")).sendKeys(datatable.cell(1, 1));
		driver.findElement(By.id("input-password")).sendKeys(Keys.ENTER);
	}
	
	@Then("je vérifie le succes de la connexion")
	public void je_verifie_le_succes_de_la_connexion() {
		
		// url va changer en cas de succès de la connexion
		String urlExpected = "http://tutorialsninja.com/demo/index.php?route=account/account";
		
		// Création de la variable qui génère l'url actuelle
	    String urlCurrent = driver.getCurrentUrl();
	    
	  //****On va vérifier si l'option Logout existe***
	    // on va cliquer sur My Account
	    driver.findElement(By.xpath("//span[text()='My Account']")).click();
	    //vérifier si link Logout existe
    	String linkCurrent = driver.findElement(By.xpath("//a[text()='Logout']")).getText();
    	
    	//cette variable est pour le texte attendu
    	String linkExpected = "Logout";
    	
    	if((!(linkCurrent.equals(linkExpected))) || (!(urlExpected.equals(urlCurrent)))) {
			fail();
		}
    driver.quit();	
    }
	

	@Then("je verifie l echec de la connexion")
	public void je_verifie_l_echec_de_la_connexion() {
		
		// On doit rester dans la même page dans le cas de l'échec de la connexion
		String urlExpected = "http://tutorialsninja.com/demo/index.php?route=account/login";
		
		// Création de la variable qui génère l'url actuelle
	    String urlCurrent = driver.getCurrentUrl();
	    
	  //cette variable est pour stocker le texte affiché
	    String textCurrent = driver.findElement(By.xpath("//div[contains(@class,'alert alert-danger alert-dismissible')]")).getText();
    	
    	//cette variable est pour le texte attendu
    	String textExpected = "Warning: No match for E-Mail Address and/or Password.";
    	
    	if((!(textCurrent.equals(textExpected))) || (!(urlExpected.equals(urlCurrent)))) {
			fail();
		}
    	driver.quit();
    }
	
	@When("je reinitialise mon mot de passe")
	public void je_reinitialise_mon_mot_de_passe(DataTable datatable) {
		
		//cliquer sur lien Password
		driver.findElement(By.xpath("//a[text()='Password']")).click();
		
		//Entrer le nouveau password
		driver.findElement(By.id("input-password")).sendKeys(datatable.cell(1, 0));
		
		//Confirmer le nouveau password
		driver.findElement(By.id("input-confirm")).sendKeys(datatable.cell(1, 0));
		
		//Cliquer sur continuer
		driver.findElement(By.xpath("//input[@value='Continue']")).click();
	}
	
	@Then("je valide que le mot de passe est reinitialise")
	public void je_valide_que_le_mot_de_passe_est_reinitialise() {
		
		String textCurrent = driver.findElement(By.xpath("//div[contains(@class,'alert-success')]")).getText();
		String textExpected = "Success: Your password has been successfully updated.";
		if(!(textCurrent.equals(textExpected))) {
			fail();
		}
		driver.quit();
	}
	
	@When("je clique sur Forgotten Password et je rentre mon courriel")
	public void je_clique_sur_forgotten_password_et_je_rentre_mon_courriel(DataTable datatable) {
	    
		// je clique sur Forgotten Password
		driver.findElement(By.xpath("//form/div/a[text()='Forgotten Password']")).click();
		
		//je rentre mon courriel
		driver.findElement(By.id("input-email")).sendKeys(datatable.cell(1, 0));
		
		//je clique sur continue
		driver.findElement(By.xpath("//input[@value='Continue']")).click();		
	}
	
	@Then("je valide l envoie du lien de réinitialisation du password")
	public void je_valide_l_envoie_du_lien_de_réinitialisation_du_password() {
		
		String textCurrent = driver.findElement(By.xpath("//div[contains(@class,'alert-success')]")).getText();
		String textExpected = "An email with a confirmation link has been sent your email address.";
		
		if(!(textCurrent.equals(textExpected))) {
			fail();
		}
		driver.quit();	    
	}
}

