package steps;

import static org.junit.Assert.fail;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class EcommerceHomePage_steps {
	
	WebDriver driver;
	String url="http://tutorialsninja.com/demo/";
	
	@Given("je suis sur home page de Ecommerce")
	public void je_suis_sur_home_page_de_Ecommerce() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get(url);
	}
	@When("je fais une recherche d un produit")
	public void je_fais_une_recherche_d_un_produit(DataTable datatable) {
		driver.findElement(By.xpath("//input[@name='search']")).sendKeys(datatable.cell(1, 0));
		driver.findElement(By.xpath("//button[contains(@class,'btn-default')]")).click();			
	}
	
	@Then("Je valide que le produit existe")
	public void je_valide_que_le_produit_existe(DataTable datatable){
		
		String textCurrent = driver.findElement(By.xpath("//h1[contains(.,'Search')]")).getText();
		
		String textExpected = "Search - "+datatable.cell(1, 0); 
		
		String urlExpected = "http://tutorialsninja.com/demo/index.php?route=product/search&search="+datatable.cell(1, 0);
		
	    String urlCurrent = driver.getCurrentUrl();
				
		if((!(textCurrent.equals(textExpected))) || (!(urlExpected.equals(urlCurrent)))){
			fail();
		}
		
		driver.quit();
	}
	
	@Then("Je valide que le produit n existe pas")
	public void je_valide_que_le_produit_n_existe_pas(DataTable datatable){
		
		String textCurrent = driver.findElement(By.xpath("//p[contains(.,'no product')]")).getText();
		
		String textExpected = "There is no product that matches the search criteria.";
		
		String urlExpected = "http://tutorialsninja.com/demo/index.php?route=product/search&search="+datatable.cell(1, 0);
		
	    String urlCurrent = driver.getCurrentUrl();
				
		if((!(textCurrent.equals(textExpected))) || (!(urlExpected.equals(urlCurrent)))){
			fail();
		}
		
		driver.quit();
	}
	
}	