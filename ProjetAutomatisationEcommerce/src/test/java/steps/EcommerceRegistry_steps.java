package steps;
import static org.junit.Assert.fail;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

import io.cucumber.datatable.DataTable;


public class EcommerceRegistry_steps {
	WebDriver driver;
	String url="http://tutorialsninja.com/demo/index.php?route=account/register";
	
	@Given("Je suis dans la page Register de Ecommerce")
	public void je_suis_dans_la_page_register() {
		WebDriverManager.chromedriver().setup();
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get(url);
	}
	
	@When("je remplis le formulaire")
	
	//S'enregistrer en remplissant tous les champs obligatoires
	public void je_rempli_le_formulaire (DataTable datatable) {
		
		//Remplir le champ firstName
		if(!datatable.cell(1, 0).equalsIgnoreCase("[Blank]")) {
			driver.findElement(By.id("input-firstname")).sendKeys(datatable.cell(1, 0));
		}
		
		//Remplir le champ name
		if(!datatable.cell(1, 1).equalsIgnoreCase("[Blank]")) {
			driver.findElement(By.id("input-lastname")).sendKeys(datatable.cell(1, 1));
		}
		
		//Remplir le champ email
		if(!datatable.cell(1, 2).equalsIgnoreCase("[Blank]")) {
			driver.findElement(By.id("input-email")).sendKeys(datatable.cell(1, 2));
		}
		
		//Remplir le champ telephone
		if(!datatable.cell(1, 3).equalsIgnoreCase("[Blank]")) {
			driver.findElement(By.id("input-telephone")).sendKeys(datatable.cell(1, 3));
		}
		
		//Remplir le champs password
		if(!datatable.cell(1, 4).equalsIgnoreCase("[Blank]")) {
			driver.findElement(By.id("input-password")).sendKeys(datatable.cell(1, 4));
		}
		
		//Remplir le champ de confirmation de password
		if(!datatable.cell(1, 5).equalsIgnoreCase("[Blank]")) {
			driver.findElement(By.id("input-confirm")).sendKeys(datatable.cell(1, 5));
		}
		
		// choisir de souscrire à la newsletter
		if (datatable.cell(1, 6).equalsIgnoreCase("oui")){
			driver.findElement(By.xpath("//label[1]/input[@name='newsletter']")).click();
		}
		
		//Click sur checkbox pour accepter les conditions
		if(datatable.cell(1, 7).equalsIgnoreCase("oui")){
			driver.findElement(By.name("agree")).click();
		}
		
		// cliquer sur submit
		driver.findElement(By.xpath("//input[@type='submit']")).click();
		
}
	
	@Then("je vérifie le succès de l inscription")
	
	public void je_verifie_le_succes_de_l_inscription () {
		
		// Voici l'url où on doit être dans le cas où l'inscription est réussie
		String urlExpected = "http://tutorialsninja.com/demo/index.php?route=account/success";
		
		// Création de la variable qui génère l'url actuelle
	    String urlCurrent = driver.getCurrentUrl();
	    
	    //Création d'une variable pour stocker le texte affiché 
    	String textCurrent = driver.findElement(By.xpath("//h1[contains(text(),'Has Been Created!')]")).getText();
    	
    	//Voici la variable qui contient le texte attendu
    	String textExpected = "Your Account Has Been Created!";
    	
    	//Si le texte affiché ou l'url n'est pas bon, le test doit échouer     	
    	if (!(textCurrent.equals(textExpected)) || (!urlExpected.equals(urlCurrent))) {
    		fail();			
    	}
    	
    	driver.quit();
	}
    	
	@Then("je vérifie l inscription à la newsletters")
	
	public void je_vérifie_l_inscription_a_la_newsletters() {
		
		// je clique sur Continuer après avoir été inscrit
		driver.findElement(By.xpath("//a[text()='Continue']")).click();
		
		//je clique sur Subscribe / unsubscribe to newsletter dans la section newsletter
		driver.findElement(By.xpath("//a[text()='Subscribe / unsubscribe to newsletter']")).click();
		
		//varible contient le localisateur du bouton Yes devant Subscribe
		WebElement btnYes = driver.findElement(By.xpath("//label[1]/input[@name='newsletter']"));
		
		//si le Yes n'est pas selectionné, échouer le test
		if(!btnYes.isSelected()) {
			fail();
		}
		driver.quit();	
	}
	
	@Then("je vérifie l echec de l inscription suite au champ obligatoire vide")
		
	public void je_verifie_l_echec_de_l_inscription_champ_obligatoire_vide (DataTable datatable) {		
	    
		//voici l'url où la page ne doit pas se diriger car c'est une url dans le cas d'un succès de l'inscription
		String urlNotExpected = "http://tutorialsninja.com/demo/index.php?route=account/success";
		
		// Création de la variable qui génère l'url actuelle
	    String urlCurrent = driver.getCurrentUrl();
	    
	    // Si le champ first name est laissé vide
	    if(datatable.cell(1, 0).equals("")){
	    	
	    	//cette variable est pour stocker le texte affiché
	    	String textCurrent = driver.findElement(By.xpath("//div[contains(text(),'First Name must be')]")).getText();
	    	
	    	//cette variable est pour le texte attendu
	    	String textExpected = "First Name must be between 1 and 32 characters!"; 
	    	
	    	//Si le texte affiché ou l'url n'est pas bon, le test doit échoué
	    	if((!(textCurrent.equals(textExpected))) || (urlNotExpected.equals(urlCurrent))) {
				fail();
			}
	    }
	    
	    //Sinon si le champ last name est laissé vide
	    else if(datatable.cell(1, 1).equals("")){
	    	
	    	//cette variable est pour stocker le texte affiché
	    	String textCurrent = driver.findElement(By.xpath("//div[contains(text(),'Last Name must be')]")).getText();
	    	
	    	//cette variable est pour le texte attendu
	    	String textExpected = "Last Name must be between 1 and 32 characters!"; 
	    	
	    	//Si le texte affiché ou l'url n'est pas bon, le test doit échouer
	    	if(!(textCurrent.equals((textExpected))) || (urlNotExpected.equals(urlCurrent))) {
				fail();
			}
	    }
	    
	  //Sinon si le champ email est laissé vide
	    else if(datatable.cell(1, 2).equals("")){	
	    	
	    	//cette variable est pour stocker le texte affiché
	    	String textCurrent = driver.findElement(By.xpath("//div[contains(text(),'E-Mail Address does not')]")).getText();
	    	
	    	//cette variable est pour le texte attendu
	    	String textExpected = "E-Mail Address does not appear to be valid!";
	    	
	    	//Si le texte affiché ou l'url n'est pas bon, le test doit échouer
	    	if((!(textCurrent.equals(textExpected))) || (urlNotExpected.equals(urlCurrent))) {
		    	fail();    
		    }
	    }   
		
	    
	  //Sinon si le champ telephone est laissé vide
	    else if(datatable.cell(1, 3).equals("")){
	    	
	    	//cette variable est pour stocker le texte affiché
	    	String textCurrent = driver.findElement(By.xpath("//div[contains(text(),'Telephone must be')]")).getText();
	    	
	    	//cette variable est pour le texte attendu
	    	String textExpected = "Telephone must be between 3 and 32 characters!";
	    	
	    	//Si le texte affiché ou l'url n'est pas bon, le test doit échouer
	    	if((!(textCurrent.equals(textExpected))) || (urlNotExpected.equals(urlCurrent))) {
		    	fail();    
		    }
	    }
	    
	  //Sinon si le champ password est laissé vide
	    else if(datatable.cell(1, 4).equals("")){
	    	
	    	//cette variable est pour stocker le texte affiché
		    String textCurrent = driver.findElement(By.xpath("//div[contains(text(),'Password must be')]")).getText();
		    
		  //cette variable est pour le texte attendu
	    	String textExpected = "Password must be between 4 and 20 characters!";
		    
	    	//Si le texte affiché ou l'url n'est pas bon, le test doit échouer
		    if((!(textCurrent.equals(textExpected))) || (urlNotExpected.equals(urlCurrent))) {
			    fail();    
			}
	    }   
	    
	  //Sinon si le champ password confirm est laissé vide
	    else if(datatable.cell(1, 5).equals("")){
	    	
	    	//cette variable est pour stocker le texte affiché
	    	String textCurrent = driver.findElement(By.xpath("//div[contains(text(),'Password confirmation does not')]")).getText();
	    		
	    	//cette variable est pour le texte attendu
	    	String textExpected = "Password confirmation does not match password!";
		    	
	    	//Si le texte affiché ou l'url n'est pas bon, le test doit échouer
		    if((!(textCurrent.equals(textExpected))) || (urlNotExpected.equals(urlCurrent))) {
			    fail();    
			}			    
	    }
	    
	    
	  //Sinon si la case agree Privacy Policy n'est pas coché ça veut dire on a donné autre chose que "oui" comme argument 
	    else if (!datatable.cell(1, 7).equalsIgnoreCase("oui")) {
	    	
	    	//cette variable est pour stocker le texte affiché
    		String textCurrent = driver.findElement(By.xpath("//div[contains(text(),' Warning: You must agree')]")).getText();
    			
    		//cette variable est pour le texte attendu
	    	String textExpected = "Warning: You must agree to the Privacy Policy!";
	    		
	    	//Si le texte affiché ou l'url n'est pas bon, le test doit échouer
    		if((!(textCurrent.equals(textExpected))) || (urlNotExpected.equals(urlCurrent))) {
		    	fail();    
    		}
	    }
	    
	    driver.quit();
	    
	}
	
	@Then("je verifie l echec de l inscription suite au courriel existant")
	public void je_verifie_l_echec_de_la_connexion_cause_courriel_existant() {
		
		//cette variable est pour stocker le texte affiché
		String textCurrent = driver.findElement(By.xpath("//div[contains(@class,'alert alert-danger alert-dismissible')]")).getText();
			
		//cette variable est pour le texte attendu
    	String textExpected = "Warning: E-Mail Address is already registered!";
    	
    	String urlNotExpected = "http://tutorialsninja.com/demo/index.php?route=account/success";
		
		// Création de la variable qui génère l'url actuelle
	    String urlCurrent = driver.getCurrentUrl();
    		
    	//Si le texte affiché ou l'url n'est pas bon, le test doit échouer
		if((!(textCurrent.equals(textExpected))) || (urlNotExpected.equals(urlCurrent))) {
	    	fail();    
		}
		driver.quit();	
    }
	
	@Then("je verifie l echec de l inscription suite au confirmPwd différend de pwd")
	public void je_verifie_l_echec_connexion_confirfPwd_different_de_pwd(DataTable datatable) {
		
		//voici l'url où la page ne doit pas se diriger car c'est une url dans le cas d'un succès de l'inscription
		String urlNotExpected = "http://tutorialsninja.com/demo/index.php?route=account/success";
				
		// Création de la variable qui génère l'url actuelle
		String urlCurrent = driver.getCurrentUrl();
			    
		if(!datatable.cell(1, 1).equals(datatable.cell(1, 0))){
	    	
			//cette variable est pour stocker le texte affiché
			String textCurrent = driver.findElement(By.xpath("//div[contains(text(),'Password confirmation does not')]")).getText();
		    		
			//cette variable est pour le texte attendu
			String textExpected = "Password confirmation does not match password!";
			    	
			//Si le texte affiché ou l'url n'est pas bon, le test doit échouer
			if((!(textCurrent.equals(textExpected))) || (urlNotExpected.equals(urlCurrent))) {
				    fail();    
			}	
		}
		driver.quit();	
    }
}
		 


