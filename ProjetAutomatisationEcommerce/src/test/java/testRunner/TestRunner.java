package testRunner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		features= {"features"},	
		glue= {"steps"},
		monochrome=true,
		dryRun=false,
		
		//tags="@RegistryChampObligatoireVide",
		plugin= {"pretty","html:rapport"}
		)
public class TestRunner {

}
