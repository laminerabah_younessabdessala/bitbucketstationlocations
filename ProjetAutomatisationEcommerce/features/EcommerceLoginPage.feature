Feature: Ecommerce Login page

  @LoginValide
  Scenario: Se connecter à l application Ecommerce avec des données valides
  
    Given je suis dans la page Login de Ecommerce
    
    When je me connecte avec les logins
    	| email | password |
    	| 2123185@bdeb.qc.ca | 12345678 |
    	
    Then je vérifie le succes de la connexion

  @LoginInvalide
  Scenario: Impossibilité de se connecter à l application Ecommerce avec des données invalides
  
    Given je suis dans la page Login de Ecommerce
    
    When je me connecte avec les logins 
    	| email | password |
    	| 2123185@bdeb.qc.ca | 87654321 |
    	 
    Then je verifie l echec de la connexion

  @ResetPwdApartirDeMonCompte    
  Scenario: Réinitialiser le mot de passe d accès à Ecommerce à partir de mon compte
  
  Given je suis dans la page Login de Ecommerce
  
  And je me connecte avec les logins
  	| email | password |
    | test@hotmail.com | 87654321 |
  
  When je reinitialise mon mot de passe
  	| nouveau password |
  	| 12345678 |
  	
  Then je valide que le mot de passe est reinitialise
  
  @ResetPwdOublié    
  Scenario: Réinitialiser le mot de passe oublié pour accéder à Ecommerce
  
  Given je suis dans la page Login de Ecommerce
  
  When je clique sur Forgotten Password et je rentre mon courriel
  	| courriel |
  	| test@hotmail.com |
  	
  Then je valide l envoie du lien de réinitialisation du password
  