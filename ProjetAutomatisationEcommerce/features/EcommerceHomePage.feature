Feature: Ecommerce Home Page

  @rechercheProduitExistant
  Scenario: Rechercher un produit existant dans l application Ecommerce
    Given je suis sur home page de Ecommerce
    When je fais une recherche d un produit
    	| produit |
    	| samsung |
    Then Je valide que le produit existe
			| produit |
  		| samsung |
  		
  @rechercheProduitInexistant
  Scenario: Impossibilité de trouver un produit inexistant avec la recherche dans l application Ecommerce
    Given je suis sur home page de Ecommerce
    When je fais une recherche d un produit
    	| produit |
    	| samsungxxx |
    Then Je valide que le produit n existe pas
			| produit |
  		| samsungxxx |