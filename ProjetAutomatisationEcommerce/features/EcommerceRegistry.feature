Feature: Ecommerce Registry Page

# l'inscription à l'application prend 8 arguments (string) pour remplir le formulaire
  # Voici les arguments:
  #1- firstName  
  #2- name
  #3- email
  #4- telephone
  #5- password
  #6- confirmation de password
  #7- l'argument doit être à "oui" si on veut cocher "Yes" devant subscribe (Newsletter)
  #8- l'argument doit être à "oui" pour cocher "I have read and agree to the Privacy Policy"
   

	@RegistryValide
  Scenario: S inscrire à l application Ecommerce en remplissant tous les champs obligatoires
   
    Given Je suis dans la page Register de Ecommerce
    
    When je remplis le formulaire
    	| firstName | lastName | courriel | telephone | pwd | confirm pwd | newsletter | agreePrivacyPolicy |
    	| TestPrenom | Testnom | courriel.test2222@outlook.com | 435-984-6542 | La12345 | La12345 | non | oui |
    	
    Then je vérifie le succès de l inscription
    	
  @RegistryWithNewsletter
  Scenario: S inscrire à l application Ecommerce en s abonnant à la newsletter
   # on répond à Oui pour le 7eme paramètre
   
    Given Je suis dans la page Register de Ecommerce
    
    When je remplis le formulaire
    	| firstName | lastName | courriel | telephone | pwd | confirm pwd | newsletter | agreePrivacyPolicy |
    	| TestPrenom | Testnom | courriel.test21212@outlook.com | 435-984-6542 | La12345 | La12345 | oui | oui |
    	
    Then je vérifie l inscription à la newsletters
  
  @RegistryCompteExistant
  Scenario: Impossiblité de s inscrire à l application Ecommerce avec un compte existant
  # Donc avec courriel existant
  
    Given Je suis dans la page Register de Ecommerce
    
    When je remplis le formulaire
    	| firstName | lastName | courriel | telephone | pwd | confirm pwd | newsletter | agreePrivacyPolicy |
    	| PrenomtTest | NomtTest | test@hotmail.com | 435-984-6542 | La12345 | La12345 | non | oui |
    	
    Then je verifie l echec de l inscription suite au courriel existant
    
  @RegistryConfirmPwdDifferntDePwd
  Scenario: Impossiblité de s inscrire à l application Ecommerce si confirm password et différent du password
  
  	Given Je suis dans la page Register de Ecommerce
    
    When je remplis le formulaire
    	| firstName | lastName | courriel | telephone | pwd | confirm pwd | newsletter | agreePrivacyPolicy |
    	| testTest | Test | testCourriel1023@yahoo.ca | 435-654-6542 | test12345 | pwd1234 | non | oui |
    	
    Then je verifie l echec de l inscription suite au confirmPwd différend de pwd
    	| pwd | confirm pwd |
    	| test12345 | pwd1234 |

  @RegistryChampObligatoireVide
  Scenario Outline: Impossiblité de s inscrire à l application Ecommerce en laissant un champ obligatoire vide
   
    Given Je suis dans la page Register de Ecommerce
    
    When je remplis le formulaire
    	| firstName | name | email | telephone | pwd | confirmPwd | newsletter | agreePolicy |
    	| <firstName> | <name> | <email> | <telephone> | <pwd> | <confirmPwd> | <newsletter> | <agreePolicy> |
    	
    Then je vérifie l echec de l inscription suite au champ obligatoire vide
			| firstName | name | email | telephone | pwd | confirmPwd | newsletter | agreePolicy |
    	| <firstName> | <name> | <email> | <telephone> | <pwd> | <confirmPwd> | <newsletter> | <agreePolicy> |
    	
    Examples:
    | firstName | name | email | telephone | pwd | confirmPwd | newsletter | agreePolicy |
    | [Blank] | Dubois | dalain@yahoo.ca | 435-654-6542 | test12345 | test12345 | non | oui |
    | Alain | [Blank] | dalain@yahoo.ca | 435-654-6542 | test12345 | test12345 | non | oui |
    | Alain | Dubois | [Blank] | 435-654-6542 | test12345 | test12345 | non | oui |
    | Alain | Dubois | dalain@yahoo.ca | [Blank] | test12345 | test12345 | non | oui |
    | Alain | Dubois | dalain@yahoo.ca | 435-654-6542 | [Blank] | test12345 | non | oui |
    | Alain | Dubois | dalain@yahoo.ca | 435-654-6542 | test12345 | [Blank] | non | oui |    
    | Alain | Dubois | dalain@yahoo.ca | 435-654-6542 | test12345 | test1234 | non |non|
       